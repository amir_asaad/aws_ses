# Campaign

Django app for using Amazon Simple Email Service (SES) for email camign.

### What is this repository for? 

Django app for using Amazon Simple Email Service (SES) for email marketing. This will let you keep track with bounces and complains emails for Amazon Simple Notification Service (SNS),
Also huge email database can be imported.

## Getting started

```bash
$ pip install -r requirements.txt
```

### Settings.py
```env
EMAIL_BACKEND = 'django_ses.SESBackend'
DEFAULT_FROM_EMAIL = 'Sample <sample@company.com>'

AWS_ACCESS_KEY_ID = 'Amazon Access Key'
AWS_SECRET_ACCESS_KEY = 'Amazon Secret Access Key'

# Additionally, you can specify an optional region, like so:
AWS_SES_REGION_NAME = 'us-east-1'
AWS_SES_REGION_ENDPOINT = 'email.us-east-1.amazonaws.com'

# Alternatively, instead of AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
# AWS_SES_ACCESS_KEY_ID = ''
# AWS_SES_SECRET_ACCESS_KEY = ''

AWS_SES_AUTO_THROTTLE = 0.5  # (default; safety factor applied to rate limit)

BOUNCY_TOPIC_ARN = [
    
]
```



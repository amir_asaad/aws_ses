from django.conf.urls import patterns, url

from .views import open_tracking, unsubscribe

urlpatterns = patterns(
    '',
    url(r'^open-tracking/', open_tracking),
    url(r'^unsubscribe/', unsubscribe)
)

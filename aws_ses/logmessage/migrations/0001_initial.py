# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EmailTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('html', models.TextField()),
                ('text', models.TextField()),
                ('description', models.TextField()),
                ('category', models.CharField(max_length=100)),
                ('created', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-created'],
                'verbose_name': 'Email Template',
                'verbose_name_plural': 'Email Templates',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LogMessege',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sender', models.EmailField(max_length=75)),
                ('to', models.EmailField(max_length=75)),
                ('created', models.DateTimeField(auto_now=True)),
                ('template', models.ForeignKey(to='logmessage.EmailTemplate')),
            ],
            options={
                'ordering': ['-created'],
                'verbose_name': 'Log Messege',
                'verbose_name_plural': 'Log Messeges',
            },
            bases=(models.Model,),
        ),
    ]

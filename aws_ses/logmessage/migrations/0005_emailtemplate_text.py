# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('logmessage', '0004_logmessage_is_open'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailtemplate',
            name='text',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('logmessage', '0003_remove_emailtemplate_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='logmessage',
            name='is_open',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]

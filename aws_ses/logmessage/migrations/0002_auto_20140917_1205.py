# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('logmessage', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LogMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sender', models.EmailField(max_length=75)),
                ('to', models.EmailField(max_length=75)),
                ('created', models.DateTimeField(auto_now=True)),
                ('template', models.ForeignKey(to='logmessage.EmailTemplate')),
            ],
            options={
                'ordering': ['-created'],
                'verbose_name': 'Log Message',
                'verbose_name_plural': 'Log Messages',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='logmessege',
            name='template',
        ),
        migrations.DeleteModel(
            name='LogMessege',
        ),
    ]

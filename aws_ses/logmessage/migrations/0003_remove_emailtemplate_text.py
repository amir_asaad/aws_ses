# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('logmessage', '0002_auto_20140917_1205'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emailtemplate',
            name='text',
        ),
    ]

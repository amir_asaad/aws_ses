from django.http import HttpResponse, Http404
from django.conf import settings

from .models import LogMessage
from newsletters.models import Subscriber, Unsubsriber


def open_tracking(request):
    if 'id' in request.GET:
        log = LogMessage.objects.get(id=request.GET['id'])
        log.is_open = True
        log.save(update_fields=['is_open'])

    image_data = open(
        settings.STATIC_ROOT + "/images/email_track.jpg", 'rb').read()
    return HttpResponse(image_data, content_type="image/jpeg")


def unsubscribe(request):
    if 'email' in request.GET:
        email = request.GET['email']
        try:
            subscriber = Subscriber.objects.get(email=email)
        except Subscriber.DoesNotExist:
            raise Http404

        subscriber.delete()
        Unsubsriber.objects.create(email=email)
        return HttpResponse('You have successfuly unsubscribed \
            from our mailing list with email: ' + email)

    return HttpResponse()

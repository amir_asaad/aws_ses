from django.db import models


class EmailTemplate(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    html = models.TextField()
    description = models.TextField()
    category = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Email Template'
        verbose_name_plural = 'Email Templates'
        ordering = ['-created']

    def __unicode__(self):
        return '%s' % self.title


class LogMessage(models.Model):
    sender = models.EmailField()
    to = models.EmailField()
    template = models.ForeignKey(EmailTemplate)
    is_open = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Log Message'
        verbose_name_plural = 'Log Messages'
        ordering = ['-created']

    def __unicode__(self):
        return 'to: %s' % self.to

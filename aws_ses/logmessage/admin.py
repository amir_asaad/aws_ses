from django.contrib import admin

from .models import EmailTemplate, LogMessage


class EmailTemplateAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'text', 'category', 'created')


class LogMessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'to', 'template', 'is_open', 'created')

admin.site.register(EmailTemplate, EmailTemplateAdmin)
admin.site.register(LogMessage, LogMessageAdmin)

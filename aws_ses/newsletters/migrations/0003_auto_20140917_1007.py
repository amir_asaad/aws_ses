# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletters', '0002_auto_20140916_1000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='no_of_employees',
            field=models.CharField(max_length=255, null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletters', '0004_auto_20140923_0814'),
    ]

    operations = [
        migrations.CreateModel(
            name='Unsubsriber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=75)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Unsubsriber',
                'verbose_name_plural': 'Unsubsribers',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='subscriber',
            name='country_code',
        ),
    ]

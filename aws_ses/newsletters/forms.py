from django import forms

from logmessage.models import EmailTemplate


class SelectTemplateForm(forms.Form):
        _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
        template = forms.ModelChoiceField(EmailTemplate.objects)

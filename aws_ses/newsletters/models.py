from django.db import models


class Subscriber(models.Model):
    email = models.EmailField()
    sent = models.BooleanField(default=False)
    contact = models.CharField(max_length=255, null=True, blank=True)
    designation = models.CharField(max_length=255, null=True, blank=True)
    company_name = models.CharField(max_length=255, null=True, blank=True)
    nationality = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=255, null=True, blank=True)
    category = models.CharField(max_length=255, null=True, blank=True)
    activity = models.CharField(max_length=255, null=True, blank=True)
    company_profile = models.CharField(max_length=255, null=True, blank=True)
    year_of_establishment = models.CharField(
        max_length=255, null=True, blank=True)
    no_of_employees = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    city_code = models.CharField(max_length=255, null=True, blank=True)
    telephone = models.CharField(max_length=255, null=True, blank=True)
    fax = models.CharField(max_length=255, null=True, blank=True)
    mobile = models.CharField(max_length=255, null=True, blank=True)
    website = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return '%s' % self.email


class Unsubsriber(models.Model):
    email = models.EmailField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Unsubsriber'
        verbose_name_plural = 'Unsubsribers'

    def __unicode__(self):
        return '%s' % self.email

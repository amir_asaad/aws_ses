from django.contrib import admin
from django.http import HttpResponseRedirect
from django import forms
from django.shortcuts import render
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

from import_export.admin import ImportExportModelAdmin
from import_export import resources

from .models import Subscriber, Unsubsriber
from logmessage.models import EmailTemplate, LogMessage


class SubscriberResource(resources.ModelResource):

    class Meta:
        model = Subscriber
        exclude = ('id', 'sent',)
        import_id_fields = ['email']


class SupscriberAdmin(ImportExportModelAdmin):
    list_display = (
        'email', 'contact', 'designation', 'company_name',
        'nationality', 'category', 'activity'
    )
    list_filter = (
        'sent', 'contact', 'designation', 'company_name',
        'nationality', 'category', 'activity'
    )
    search_fields = ['email']
    actions = ['send_email', 'reset_sent']
    resource_class = SubscriberResource

    def reset_sent(self, request, queryset):
        queryset.update(sent=False)
        self.message_user(request, "Successfully reset flag.")
        return HttpResponseRedirect(request.get_full_path())

    reset_sent.short_description = 'Reset sent falg'

    class SelectTemplateForm(forms.Form):
        _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
        template = forms.ModelChoiceField(EmailTemplate.objects)

    def send_email(self, request, queryset):
        form = None

        if 'apply' in request.POST:
            form = self.SelectTemplateForm(request.POST)

            if form.is_valid():
                template = form.cleaned_data['template']
                from_addr = getattr(settings, 'DEFAULT_FROM_EMAIL')

                for subscriber in queryset:
                    log = LogMessage.objects.create(
                        to=subscriber.email, sender=from_addr,
                        template=template)
                    url = "http://54.85.174.111"
                    image_url = (
                        u'<img src="%s/log/open-tracking/?id=%s" />'
                    ) % (url, log.id)
                    unsubsrcibe_link = (
                        u'<a href="%s/log/unsubscribe/?email=%s">Unsubdribe</a>'
                    ) % (url, subscriber.email)
                    body = template.html + image_url + unsubsrcibe_link

                    email = EmailMultiAlternatives(
                        template.title, template.text,
                        from_addr, [subscriber.email])
                    email.attach_alternative(body, 'text/html')
                    email.send()

                queryset.update(sent=True)

                self.message_user(
                    request,
                    "Successfully sent email %s ." % template)
                return HttpResponseRedirect(request.get_full_path())

        if not form:
            form = self.SelectTemplateForm(
                initial={'_selected_action': request.POST.getlist(
                    admin.ACTION_CHECKBOX_NAME)})

        return render(request, 'admin/send_email.html', {
                      'emails': queryset,
                      'tag_form': form,
                      })

    send_email.short_description = "Send Emails from Templates"


class UnsubscriberAdmin(admin.ModelAdmin):
    list_display = ('email', 'date')
    list_filter = ('email', 'date')
    search_fields = ['email']

admin.site.register(Subscriber, SupscriberAdmin)
admin.site.register(Unsubsriber, UnsubscriberAdmin)
